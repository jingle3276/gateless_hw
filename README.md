# Gateless Homework

## Dependencies and Steup
* `python3`
  - Python 3.7.5
* `pytest`
  - pytest 6.2.1
* `flask`
  - Flask 1.1.2

## Step 0: Steup PYTHONPATH and Run unit tests
```
cd gateless_hw
PYTHONPATH=$PYTHONPATH:.
export PYTHONPATH
pytest .
```

## Step 1 Run program
### show help

  `python3 src/driver.py -h`

### sort by email(descending) then by last name ascending

  `python3 src/driver.py --dir data -f email last_name -o desc asc`

### sort by birthday(ascending)
  
  `python3 src/driver.py --dir data -f date_of_birth  -o asc`

### sort by last name(descending)
  
  `python3 src/driver.py --dir data -f last_name -o desc`

## Step 2 Run and use Api

### start api server 
  
  `python3 src/api_server.py`

### GET /records/email

  `http://localhost:5000/records/last_name?desc`

  `http://localhost:5000/records/last_name?asc`

### GET /records/date_of_birth

  `http://localhost:5000/records/date_of_birth`

### GET /records/last_name or /records/fisrt_name

  `http://localhost:5000/records/last_name?desc`

  `http://localhost:5000/records/first_name?asc`

### POST /records

  `http://localhost:5000/records`

  **POST payload must be json and cotains these two fields**
  
   ```
   { 
     "line": "Biden, Joseph, Joe.Biden@wh.gov, Blue, 11/22/1942",
	 "line_type": ".csv" 
   }
   ```
