# Data Model of record
import re 
from datetime import datetime
import json


class Record():

    def __init__(self, last_name, first_name, email, favorite_color, date_of_birth):
        assert self._validate_string(last_name) 
        assert self._validate_string(first_name) 
        assert self._validate_email(email)  
        assert self._validate_string(favorite_color)
        assert self._validate_dob(date_of_birth)

        self.last_name = last_name
        self.first_name = first_name
        self.email = email
        self.favorite_color = favorite_color
        self.date_of_birth = self._parse_date_time(date_of_birth)
  

    def __eq__(self, other):
        if isinstance(other, Record):
            return self.first_name == other.first_name and \
                    self.last_name == other.last_name and \
                    self.email == other.email and \
                    self.favorite_color == other.favorite_color and \
                    self.date_of_birth == other.date_of_birth
        return False


    def __str__(self):
        dob_str = self.date_of_birth.strftime("%m/%d/%Y")
        return "{:12} {:12} {:32} {:10} {:10}".format(self.first_name, self.last_name, self.email, self.favorite_color, dob_str)


    def __hash__(self):
        return hash(self.email)


    def to_dict(self):
        return {
                "first_name": self.first_name,
                "last_name": self.last_name,
                "email": self.email,
                "favorite_color": self.favorite_color,
                "date_of_birthday": self.date_of_birth.strftime("%m/%d/%Y")
            }
        
                
    def _validate_string(self, item):
        return len(item) > 0

    def _validate_email(self, email):
        if re.match("^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", email) != None:
            return True
        else:
            return False

    def _parse_date_time(self, datetime_str):
        return datetime.strptime(datetime_str, "%m/%d/%Y").date()

    def _validate_dob(self, datetime_str):
        try:
            self._parse_date_time(datetime_str)
            return True
        except ValueError as ve:
            return False

