from flask import Flask, request, jsonify, abort

from src.record import Record
from src.sorter import sort
from src.parse_data import parse_line, get_delimiter, load_files
import json


app = Flask(__name__)
#app.debug = True

# Loads initial data from data folder
records = set(load_files("data"))

@app.route('/records/<field_name>', methods=['GET'])
def sort_records(field_name):
    desc = True
    if request.args and "asc" in request.args:
        desc = False
    sorted_records = sort(records, field_name, desc)
    return jsonify([s.to_dict() for s in sorted_records])


@app.route('/records', methods=['POST'])
def add_record():
    if not request.is_json:
        abort(400)
    json = request.get_json()
    if not json.get("line") or not json.get("line_type"):
        abort(400)

    line = json["line"]
    line_type = json["line_type"]
    record = parse_line(line, get_delimiter(line_type))
    records.add(record)
    return jsonify (record.to_dict()), 201


if __name__ == '__main__':
    app.run()
