#!/usr/bin/env python3

import argparse
from src.parse_data import load_files
from src.sorter import sort

parser = argparse.ArgumentParser(description=f"Records Sorting Program")
parser.add_argument("-f", "--sort_fields", required=True, action="append", nargs='+',
                        choices=["last_name", "first_name", "email", "favorite_color", "date_of_birth"],
                        help="the fields to sort by")
parser.add_argument("-o", "--sort_orders", required=True, action="append", nargs='+',
                        choices=["asc", "desc"],
                        help="descending or ascending. the length must match sort_field",
                        )
parser.add_argument("-d", "--dir", metavar="<dir>", required=True,
                    help="source data directory")


def display_records(records):
    for r in records:
        print(str(r))


def run_program():
    records = load_files(args.dir)
    if records:
        for sort_field, sort_order in zip(args.sort_fields[0], args.sort_orders[0]):
            records = sort(records, sort_field, sort_order=="desc")
        display_records(records)


parser.parse_args()
args = parser.parse_args()
run_program()

