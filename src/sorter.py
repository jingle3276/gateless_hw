from src.record import Record
from operator import attrgetter


def sort(records, field_name, desc=True):
    return sorted(records, key=attrgetter(field_name), reverse=desc)

