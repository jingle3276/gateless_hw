from src.record import Record
from typing import List
from os import listdir
from os.path import isfile, join, splitext


def get_delimiter(file_ext):
    assert file_ext in [".csv", ".pipe", ".space"]
    return {
        ".csv": ", ",
        ".pipe": " | ",
        ".space": " "
    }.get(file_ext)


def parse_line(line: str, delimiter: str) -> Record:
    fields = line.split(delimiter)
    assert len(fields) == 5
    return Record(fields[0], fields[1], fields[2], fields[3], fields[4])
    

# parse csv file and output a list of records
def parse_file(file_path, delimiter) -> List[Record]:
    with open(file_path) as file:
        lines = file.readlines()
        for l in lines[1:]:
            yield parse_line(l.rstrip(), delimiter)


def load_files(dir_path):
    files = [f for f in listdir(dir_path) if isfile(join(dir_path, f))]
    out = []
    for f in files:
        filename, file_ext = splitext(f)
        delimiter = get_delimiter(file_ext)
        if delimiter:
            for record in parse_file(join(dir_path, f), delimiter):
                out.append(record)
    return out