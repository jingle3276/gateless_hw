import pytest
from src.sorter import sort
from src.parse_data import parse_line


@pytest.fixture
def records():
    lines = [
        "Jackson Andrew Andrew.Jackson@wh.gov Yellow 6/8/1845",
        "Roosevelt Franklin Franklin.Roosevelt@wh.gov White 4/12/1945",
        "Washington George George.Washington@wh.gov Blue 2/22/1732",
        "Adams John John.Adams@wh.gov Red 10/30/1735",
        "Jefferson Thomas Thomas.Jefferson@wh.gov Brown 4/13/1743",
        "Madison James James.Madison@wh.gov Red 6/28/1836"
    ]
    return [parse_line(line, " ") for line in lines]


def verify_sorted(records, field, is_desc):
    prev = records[0]
    for r in records[1:]:
        if is_desc:
            assert getattr(prev, field) >= getattr(r, field)
        else:
            assert getattr(prev, field) <= getattr(r, field)


def test_sort_last_name(records):
    result = sort(records, "last_name", True)
    assert len(result) == len(records)
    verify_sorted(result, "last_name", True)

def test_sort_dob_name(records):
    result = sort(records, "date_of_birth", False)
    assert len(result) == len(records)
    verify_sorted(result, "date_of_birth", False)

def test_sort_then_sort(records):
    sorted_result1 = sort(records, "email", True)
    sorted_result2 = sort(sorted_result1, "last_name", False)

    assert len(sorted_result2) == len(sorted_result1) == len(records)
    verify_sorted(sorted_result1, "email", True)
    verify_sorted(sorted_result2, "last_name", False)

