from src.record import Record
import pytest


@pytest.fixture
def valid_emails():
    return ["abc@example.com", "abc.ef@example.com", "ab-cd66@h.com"]


@pytest.fixture
def invalid_emails():
    return ["abc.com", "@example.com"]


@pytest.fixture
def valid_birthdays():
    return ["06/07/1984", "7/4/1784"]


@pytest.fixture
def invalid_birthdays():
    return ["abc", "13/13/1900", "02-03-1900"]


def get_record(email, birthday):
    return Record("first", "last", email, "color", birthday)


def test_init_validate_email(valid_emails):
    records = [get_record(email, "06/06/1984") for email in valid_emails]
    assert len(records) == len(valid_emails)


def test_init_invalidate_emailin(invalid_emails):    
    with pytest.raises(AssertionError):
        [get_record(email, "06/06/1984") for email in invalid_emails]


def test_init_validate_birthdays(valid_birthdays):
    records = [get_record("abc@example.com", birthday) for birthday in valid_birthdays]
    assert len(records) == len(valid_birthdays)


def test_init_invalidate_birthdays(invalid_birthdays):
    with pytest.raises(AssertionError):
        [get_record("abc@example.com", birthday) for birthday in invalid_birthdays]

