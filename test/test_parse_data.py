import pytest 
from src.parse_data import parse_line, load_files
from src.record import Record
from datetime import datetime


@pytest.fixture
def record():
    return Record("Washington", "George", "George.Washington@wh.gov", "Blue", "2/22/1732")


def test_parse_line_pipe(record):
    line = "Washington | George | George.Washington@wh.gov | Blue | 2/22/1732"
    result = parse_line(line, " | ")
    assert result.last_name == record.last_name
    assert result.first_name == record.first_name
    assert result.favorite_color == record.favorite_color
    assert result.email == record.email
    assert result.date_of_birth == record.date_of_birth


def test_parse_line_comma(record):
    line = "Washington, George, George.Washington@wh.gov, Blue, 2/22/1732"
    result = parse_line(line, ", ")
    assert result == record


def test_parse_line_comma_1(record):
    line = "Washington, George, George.Washington@wh.gov, Blue, 2/22/1732"
    result = parse_line(line, ", ")
    assert result == record

def test_parse_line_space(record):
    line = "Washington George George.Washington@wh.gov Blue 2/22/1732"
    result = parse_line(line, " ")
    assert result == record


def test_load_files():
    records = load_files("data")
    for r in records:
        assert isinstance(r, Record)
    assert len(records) == 12
